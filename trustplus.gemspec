Gem::Specification.new do |spec|
  spec.name     = 'trustplus'
  spec.version  = '1.0.0'
  spec.authors  = ['Trust+']
  spec.email    = 'support@trust.plus'
  spec.summary  = 'Ruby API binding for Trust+'
  spec.homepage = 'https://trust.plus/'
  spec.license  = 'MIT'

  spec.required_ruby_version = '>= 2.0'
  spec.files         = ['lib/trustplus.rb']
  #spec.require_paths = ['lib']
end
