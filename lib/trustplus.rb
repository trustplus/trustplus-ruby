require 'net/http'
require 'openssl'
require 'uri'
require 'json'

module TrustPlus
  # The default Trust+ endpoint.
  DEFAULT_ENDPOINT = 'https://api.trust.plus/check'

  VERSION = '1.0.0'

  # TrustPlus API client.
  #
  # Instantiate the Client object with an options hash:
  #
  #   client = TrustPlus::Client.new {
  #     api_key: 'your API key'
  #   }
  #
  # Make a check:
  #
  #   result = client.check {
  #     ip: '192.0.2.1',
  #     email: 'user@example.com'
  #   }
  #
  # See the API documentation at <https://trust.plus/apidocs> for details of
  # supported request and response items.
  class Client
    def initialize(options)
      endpoint = options[:endpoint]
      if not endpoint
        endpoint = DEFAULT_ENDPOINT
      end
      @endpoint = URI.parse(endpoint)
      @apikey = options[:api_key]
      @ssl_no_verify = options[:ssl_no_verify]
    end

    # Make a Trust+ check. Pass an arguments hash, which must oontain at least
    # ip or email or both. Returns the result.
    def check(args)
      _call('trustplus.Check', args)
    end

    private
    def _call(method, args)
      body = { 'method' => method, 'params' => args, 'id' => 1, 'jsonrpc' => '2.0' }.to_json
      res = JSON.parse(_post(body))
      raise JSONRPCError, res['error'] if res['error']
      res['result']
    end

    private
    def _post(body)
      http = Net::HTTP.new(@endpoint.host, @endpoint.port)
      http.use_ssl = @endpoint.scheme == 'https'
      if @ssl_no_verify
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      req = Net::HTTP::Post.new(@endpoint.request_uri)
      req.basic_auth @apikey, ''
      req.content_type = 'application/json-rpc'
      req.body = body
      http.request(req).body
    end

    public
    class JSONRPCError < RuntimeError
    end
  end
end
